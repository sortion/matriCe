# Install matriCe

## Requirements 

* git
* cmake
* gcc

## Clone repository

```bash
git clone https://framagit.org/UncleSamulus/matriCe.git
cd matriCe
```

## Build matriCe

```bash
make
```

## Run matriCe

```bash
./bin/matriCe
```